﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace GZipTest
{
    /// <summary>
    /// Позволяет выполнить компрессию и декомпрессию файлов 
    /// </summary>
    class Compressor : IDisposable
    {
        //Входящий файл
        private FileStream inputSourceStream;
        //Путь входящего файла
        private string inputPath;
        //Результирующий
        private FileStream resultSourceStream;
        //Путь результирующего файла
        private string resultPath;
        //Номер в очереди
        public int turnId;
        //Остаток
        private double residual;
        //Вычисляемый размер
        private long size;
        //Уведомление в консоль
        private double percent;
        private double percentStart;
        private byte[] DataS;
        private byte[] CompressedData;
        //Вычисляемы размер блока
        private long bSize = Constants.Block;
        
        /// <summary>
        /// Конструктор типа Compressor
        /// </summary>
        /// <param name="inputSource">Путь к файлу</param>
        /// <param name="resultSource">Путь к результирующему файлу</param>
        public Compressor(string inputSource, string resultSource)
        {
            inputPath = inputSource;
            resultPath = resultSource;
        }

        /// <summary>
        /// Деструктор
        /// </summary>
        ~Compressor()
        {
           //Dispose();
        }

        /// <summary>
        /// Чистим временные файлы
        /// </summary>
        public void Dispose()
        {
            File.Delete(resultPath + turnId + Constants.TempFormat);
        }


        /// <summary>
        /// Компрессия файла
        /// </summary>
        /// <param name="i">Индекс потока</param>
        public void Compression(object i)
        {
            //Входящий поток
            using (inputSourceStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                //Считаем кол-во обр. блоков
                size = inputSourceStream.Length / Constants.ProcessorCount;
                double blocksCount = Convert.ToDouble(size) / Constants.Block;

                //Считаем проценты
                if (Selector.Percent == 0)
                    Selector.Percent = 100 / Constants.ProcessorCount / blocksCount;

                //Отдаем остаток
                if ((int)i == Constants.ProcessorCount - 1)
                {
                    residual = inputSourceStream.Length % Constants.ProcessorCount;
                    size += Convert.ToInt64(residual);
                }

                //Задаем позиционирование
                ThreadManager.ChangePosition(inputSourceStream, size, ref turnId);
                Thread.CurrentThread.Name = turnId.ToString();

                long pos = inputSourceStream.Position + size;
                while(inputSourceStream.Position < pos && !Selector.ExitField)
                {
                    if (Constants.Block > pos - inputSourceStream.Position)
                        bSize = pos - inputSourceStream.Position;

                    DataS = new byte[bSize];

                    inputSourceStream.Read(DataS, 0, DataS.Length);
                    using (MemoryStream memoryStream = new MemoryStream(DataS.Length))
                    {
                        using (GZipStream compressionStream = new GZipStream(memoryStream, CompressionMode.Compress))
                        {
                            try
                            {
                                compressionStream.Write(DataS, 0, DataS.Length);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Что-то пошло не так\n" + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                        CompressedData = memoryStream.ToArray();
                    }

                    //сохраняем длинну блока
                    byte[] blockSize = BitConverter.GetBytes(CompressedData.Length);
                    byte[] blockSizeFixed = new byte[4];
                    Array.Copy(blockSize, 0, blockSizeFixed, 0, blockSize.Length);

                    //Резултирующий поток
                    using (resultSourceStream = new FileStream(resultPath + turnId + Constants.TempFormat, FileMode.Append, FileAccess.Write, FileShare.Write))
                    {
                        //Задаем атрибуты результирующего файла
                        FileInfo resultFileInfo = new FileInfo(resultPath + turnId + Constants.TempFormat) { Attributes = FileAttributes.Temporary | FileAttributes.Hidden };

                        resultSourceStream.Write(blockSizeFixed, 0, blockSizeFixed.Length);
                        resultSourceStream.Write(CompressedData, 0, CompressedData.Length);
                    }
                    Console.Write("\r{0}%   ", Math.Floor(Selector.StartPercent += Selector.Percent));
                }
            }
        }



        /// <summary>
        /// Декомпрессия файла
        /// </summary>
        /// <param name="i">Индекс потока</param>
        public void Decompression(object i)
        {
            //Вычисляемый размер блока
            int blockLength;
            byte[] buffer = new byte[4];
            int globalBlockLength = 0;
            
            //Входящий поток
            using (inputSourceStream = new FileStream(inputPath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                //Задаем позицию
                ThreadManager.ChangePosition(inputSourceStream, ref globalBlockLength, ref turnId);
                Thread.CurrentThread.Name = turnId.ToString();

                //Считаем кол-во обр. блоков
                long percent = inputSourceStream.Length / Constants.ProcessorCount;
                double blocksCount = Convert.ToDouble(size) / Constants.Block;

                //Считаем проценты
                if (Selector.Percent == 0)
                    Selector.Percent = 100 / Constants.ProcessorCount / blocksCount;

                size = inputSourceStream.Position + globalBlockLength;
                while (inputSourceStream.Position < size && !Selector.ExitField)
                {
                    inputSourceStream.Read(buffer, 0, 4);
                    blockLength = BitConverter.ToInt32(buffer, 0);

                    CompressedData = new byte[blockLength];
                    
                    //Читаем сжатые блоки
                    inputSourceStream.Read(CompressedData, 0, CompressedData.Length);

                    //Читаем сжатые блоки
                    using (resultSourceStream = new FileStream(resultPath + turnId + Constants.TempFormat, FileMode.Append, FileAccess.Write, FileShare.Write))
                    using (MemoryStream memoryStream = new MemoryStream(CompressedData))
                    {
                        //Поток для записи восстановленного файла
                        using (GZipStream decompressionStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                        {
                            try
                            {
                                decompressionStream.CopyTo(resultSourceStream);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("Что-то пошло не так\n" + ex.Message + "\n" + ex.StackTrace);
                            }
                        }
                    }
                    Console.Write("\r{0}%   ", Math.Floor(Selector.StartPercent += Selector.Percent));
                }
            }
        }
    }
}