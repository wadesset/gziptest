﻿using System;

namespace GZipTest
{
    /// <summary>
    /// Константы для установки параметров при компрессии/декомпрессии
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Размер одного блока
        /// </summary>
        public const int Block = 1000000;

        /// <summary>
        /// Метод компрессии
        /// </summary>
        public const string Compress = "compress";

        /// <summary>
        ///Метод компрессии 
        /// </summary>
        public const string Decompress = "decompress";

        /// <summary>
        ///Расширение файла 
        /// </summary>
        public const string Format = ".alex";

        /// <summary>
        ///Кол-во процессоров 
        /// </summary>
        public static int ProcessorCount = Environment.ProcessorCount;

        /// <summary>
        /// Формат временного файла
        /// </summary>
        public static string TempFormat = ".tmp_alex";

        /// <summary>
        /// Помощь
        /// </summary>
        public const string Help =
            "\r\nНеобходимо ввести три параметра: команда компрессии(\"compress\" или \"decompress\"), пусть к целевому файлу,\n" +
            "путь результирующего файла.\n" +
            "\r\nПример компрессии: GZipTest.exe compress [имя исходного файла] [имя результирующего файла]\n" +
            "Пример декомпрессии: GZipTest.exe decompress [имя исходного файла] [имя результирующего файла]\n" +
            "Компрессированный файл должен иметь расширение " + Format + "\n" +
            "Для завершения работы программы нажмите Ctrl + C";
    }
}
