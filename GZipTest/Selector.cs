﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.AccessControl;
using System.Threading;
using ThreadState = System.Threading.ThreadState;

namespace GZipTest
{
    /// <summary>
    /// Позволяет выполнить компрессию и декомпрессию файлов 
    /// </summary>
    class Selector
    {
        //Массив потоков
        private static Thread[] threadMassive;
        //Флаг, завершали ли мы программу
        public static bool ExitField { get; set; } = false;
        // Вывод прогресса в консоль
        public static double StartPercent { get; set; } = 0;
        //Уведомление в консоль
        public static double Percent { get; set; }
        private static Compressor[] compressor = new Compressor[Constants.ProcessorCount];
        //Один результирующий поток
        public static FileStream ResultFileStream { get; set; }
        //Глобальная позиция
        public static long Position {get; set;} = 0;

        //Ставим метку на выход
        public static void SetExitState()
        {
            ExitField = true;
        }


        /// <summary>
        /// Метод запускающий кол-во потоков, для сжатия файла
        /// Кол-во потоков равно кол-ву ядер
        /// </summary>
        /// <param name="inputSource">Используется для передачи пути к файлу</param>
        /// <param name="resultSource">Используется для передачи пути к результирующему файлу</param>
        public static void Divider(string inputSource, string resultSource)
        {
            try
            {
                Console.WriteLine("\r\nКомпрессия");
                threadMassive = new Thread[Constants.ProcessorCount];
                for (int i = 0; i < Constants.ProcessorCount && !ExitField; i++)
                {
                    compressor[i] = new Compressor(inputSource, resultSource);
                    //Создаем и запускаем поток
                    threadMassive[i] = new Thread(compressor[i].Compression);
                    threadMassive[i].Start(i);
                }

                //Смотрим, закончили потоки работу или нет
                using (ResultFileStream = new FileStream(resultSource, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    for (int i = 0; i < threadMassive.Length && !ExitField;)
                    {
                        Thread thread = Array.Find(threadMassive, element => element.ThreadState == ThreadState.Stopped && element.Name == i.ToString());
                        if (thread != null)
                        {
                            //Путь до результирующего файла
                            string path = resultSource + thread.Name + Constants.TempFormat;
                            using (FileStream resultPartStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                byte[] blockSize = BitConverter.GetBytes(resultPartStream.Length);
                                byte[] blockSizeFixed = new byte[8];
                                Array.Copy(blockSize, 0, blockSizeFixed, 0, blockSize.Length);

                                ResultFileStream.Write(blockSizeFixed, 0, blockSizeFixed.Length);
                                resultPartStream.CopyTo(ResultFileStream);
                            }

                            compressor.First(c => c.turnId == i).Dispose();
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write("Произошла ошибка: " + ex.Message);
                Console.Write(ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Метод запускающий кол-во потоков, для разжатия файла
        /// Кол-во потоков равно кол-ву ядер
        /// </summary>
        /// <param name="inputSource">Используется для передачи пути к файлу</param>
        /// <param name="resultSource">Используется для передачи пути к результирующему файлу</param>
        public static void Joiner(string inputSource, string resultSource)
        {
            try
            {
                Console.WriteLine("\r\nДекомпрессия");
                threadMassive = new Thread[Constants.ProcessorCount];
                for (int i = 0; i < Constants.ProcessorCount && !ExitField; i++)
                {
                    compressor[i] = new Compressor(inputSource, resultSource);
                    //Создаем и запускаем птоки
                    threadMassive[i] = new Thread(compressor[i].Decompression);
                    threadMassive[i].Start(i);
                }

                using (ResultFileStream = new FileStream(resultSource, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    for (int i = 0; i < threadMassive.Length && !ExitField;)
                    {
                        Thread thread = Array.Find(threadMassive, element => element.ThreadState == ThreadState.Stopped && element.Name == i.ToString());
                        if (thread != null)
                        {
                            //Путь до результирующего файла
                            string path = resultSource + thread.Name + Constants.TempFormat;
                            using (FileStream resultPartStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                            {
                                resultPartStream.CopyTo(ResultFileStream);
                            }
                            compressor.First(c => c.turnId == i).Dispose();
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR:" + ex.Message);
                Console.WriteLine(ex.StackTrace);
                throw;
            }
        }
    }
}
