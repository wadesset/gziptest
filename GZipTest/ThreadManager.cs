﻿using System;
using System.IO;

namespace GZipTest
{
    /// <summary>
    /// Манагер потоков
    /// </summary>
    class ThreadManager
    {
        //Объект синхронизации доступа к разделяемому ресурсу
        private static readonly object Locker = new object();
        private static byte[] globalBuffer = new byte[8];
        private static int counter;


        /// <summary>
        /// Задает позицию в потоке
        /// </summary>
        /// <param name="inputSourceStream">Поток жертва(разделяемый ресурс)</param>
        /// <param name="size">Длинна позиции</param>
        /// <param name="turnId">Номер в очереди</param>
        public static void ChangePosition(FileStream inputSourceStream, long size, ref int turnId)
        {
            lock (Locker)
            {
                inputSourceStream.Seek(Selector.Position, SeekOrigin.Begin);
                Selector.Position += size;
                turnId = counter;
                counter = counter + 1;
            }
        }


        /// <summary>
        /// Задает позицию в потоке
        /// </summary>
        /// <param name="inputSourceStream">Поток жертва(разделяемый ресурс)</param>
        /// <param name="globalBlockLength">Глобальная длинна</param>
        /// <param name="turnId">омер в очереди</param>
        public static void ChangePosition(FileStream inputSourceStream, ref int globalBlockLength, ref int turnId)
        {
            lock (Locker)
            {
                inputSourceStream.Seek(Selector.Position, SeekOrigin.Begin);
                
                //Читаем первые 8 байт для получения глоб. размера
                inputSourceStream.Read(globalBuffer, 0, 8);
                globalBlockLength = BitConverter.ToInt32(globalBuffer, 0);
                Selector.Position += globalBlockLength + 8;
                turnId = counter;
                counter = counter + 1;
            }
        }
    }
}