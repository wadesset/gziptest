﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using NUnit.Framework;

namespace GZipTest.Test
{
    [TestFixture]
    class TestProgram
    {
        private delegate void compress(string inputSource, string resultSource);
        
        //Входные пар-ры для тестов
        private const string method = "compress";
        private static string inputSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source.exe";
        private static string resultSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source.alex";
        //private string inputSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source.alex";
        //private string resultSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source2";
        
        //Кол-во ядер
        private static int processorCount = Environment.ProcessorCount;
        //Сжатые байты
        private static byte[][] compressedData = new byte[processorCount][];
        //Простые байты
        private static byte[][] data = new byte[processorCount][];
        //Массив потоков
        static Thread[] threadMassive;
        //Флаг, завершали ли мы программу
        private static bool _cancelled = false;


        [Test]
        public void TestMain()
        {
            string[] args = new[] { method, inputSource, resultSource };
            InputValidator.Validate(args);
            Dictionary<string, Delegate> dict = new Dictionary<string, Delegate>();
            //dict.Add("compress", new compress(Compressor.Compress));
            //dict.Add("decompress", new compress(Compressor.Decompress));
           // dict.Add("compress", new compress(CompressBlock));
           // dict.Add("decompress", new compress(DecompressBlock));

            foreach (var o in dict.Where(f => f.Key == args[0]))
            {
                o.Value.DynamicInvoke(inputSource, resultSource);
            }

            Console.WriteLine("Test console.");
            //Console.ReadKey(true);
        }


        [Test]
        public void CompressBlock()
        {
            inputSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\divide.exe";
            resultSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source1.alex";

            Console.WriteLine("Test compress");

            using (FileStream sourceStream = new FileStream(inputSource, FileMode.OpenOrCreate))
            {
                //Поток для записи сжатого файла
                using (FileStream targetStream = File.Create(resultSource))
                {
                    //Поток архивации
                    using (GZipStream compressionStream = new GZipStream(targetStream, CompressionMode.Compress))
                    {
                        sourceStream.CopyTo(compressionStream); // копируем байты из одного потока в другой
                        Console.WriteLine("Сжатие файла {0} завершено. Исходный размер: {1}  сжатый размер: {2}.",
                            inputSource, sourceStream.Length.ToString(), targetStream.Length.ToString());
                    }
                }
            }
        }

        [Test]
        public void DecompressBlock()
        {
            inputSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source2.alex";
            resultSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\result.exe";

            Console.WriteLine("Test decompress");
            // поток для чтения из сжатого файла
            using (FileStream sourceStream = new FileStream(inputSource, FileMode.OpenOrCreate))
        {
            // поток для записи восстановленного файла
            using (FileStream targetStream = File.Create(resultSource))
            {
                // поток разархивации
                using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                {
                    decompressionStream.CopyTo(targetStream);
                    Console.WriteLine("Восстановлен файл: {0}", resultSource);
                }
            }
        }
        }


        //Рубим файлы
        [Test]
        public static void Divider()
        {
            try
            {
                //Вычисляем кол-во блоков. Должно быть обязательно дробью, иначе остаток файла не запишется
                long partSize = Constants.Block;
                //Входящий файл
                FileStream inputSourceStream = new FileStream(inputSource, FileMode.Open);
                //Результирующий
                FileStream resultSourceStream = new FileStream(resultSource, FileMode.Append);

                var result = Convert.ToDouble(inputSourceStream.Length);
                //Кол-во блоков
                double blocksCount = result / Constants.Block;

                // Сколько уже записано
                int fileOffset = 0;

                // Сколько еще осталось записать
                long sizeRemaining = inputSourceStream.Length;

                
                // Цикл по частям
                for (int i = 0; (i < blocksCount) && (!_cancelled);)
                {
                    threadMassive = new Thread[processorCount];
                    //Цикл по потокам
                    for (int proc = 0; proc < processorCount && i < blocksCount; proc++)
                    {
                        sizeRemaining = inputSourceStream.Length - (i * partSize);
                        if (sizeRemaining < partSize)
                        {
                            partSize = sizeRemaining;
                        }
                        //Заносим, сколько надо записать
                        data[proc] = new byte[partSize];
                        //меняем позицию
                        inputSourceStream.Read(data[proc], 0, (int) partSize);

                        //Создаем и запускаем поток
                        threadMassive[proc] = new Thread(Compression);
                        threadMassive[proc].Start(proc);
                        //Берем следующий блок
                        i++;
                    }

                    //Смотрм блоки этого цикла и клеим их
                    for (int p = 0; (p < processorCount) && (threadMassive[p] != null) && (!_cancelled); p++)
                    {
                        //Ждем поток, иначе все выполнится быстрее чем они запишут compressedData
                        threadMassive[p].Join();
                        if (threadMassive[p].ThreadState == ThreadState.Stopped)
                        {
                            //сохраняем длинну блока
                            byte[] blockSize = BitConverter.GetBytes(compressedData[p].Length);
                            byte[] blockSizeFixed = new byte[4];
                            Array.Copy(blockSize, 0, blockSizeFixed, 0, blockSize.Length);

                            resultSourceStream.Write(blockSizeFixed, 0, blockSizeFixed.Length);

                            resultSourceStream.Write(compressedData[p], 0, compressedData[p].Length);
                        }
                    }
                }

                resultSourceStream.Close();
                inputSourceStream.Close();
            }
            catch (Exception ex)
            {
                Console.Write("Произошла ошибка: " + ex.Message);
                Console.Write(ex.StackTrace);
                //Console.ReadKey(true);
            }
        }


        //Клеим файлы
        [Test]
        public static void Joiner()
        {
            try
            {
                inputSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\source.alex";
                resultSource = "C:\\Users\\Matt\\source\\repos\\GZipTest\\GZipTest\\bin\\Debug\\result.exe";
                //Входящий файл
                FileStream inputSourceStream = new FileStream(inputSource, FileMode.Open);
                //Результирующий
                FileStream resultSourceStream = new FileStream(resultSource, FileMode.Append);

                int blockLength;
                byte[] buffer = new byte[4];
                byte[] lenghtBuffer = new byte[4];

                //Размер для вычисления последнего блока
                int size = 0;
                //Флаг, последний лы блок мы разжали
                Boolean notLast = true;

                while (notLast == true && !_cancelled)
                {
                    threadMassive = new Thread[processorCount];
                    for (int proc = 0; proc < processorCount && !_cancelled; proc++)
                    {
                        //Считываем заголовки, чтобы понять какой размер у блока
                        inputSourceStream.Read(buffer, 0, 4);
                        blockLength = BitConverter.ToInt32(buffer, 0);

                        compressedData[proc] = new byte[blockLength];

                        //Читаем сжатые блоки
                        inputSourceStream.Read(compressedData[proc], 0, blockLength);
                        //Смотрим 4 байта содержащие размер
                        Array.Copy(compressedData[proc], compressedData[proc].Length - 4, lenghtBuffer, 0, 4);


                        //Пишем в консоль сколько разжимается
                        Console.WriteLine("Uncompressed size: " + BitConverter.ToInt32(lenghtBuffer, 0));
                        //Если размер еще не записывался, пишем в него стандартный блок
                        if (size == 0)
                            size = BitConverter.ToInt32(lenghtBuffer, 0);
                        //Если стандартный блок(который равен кол-ву при сжатии) меньше, вешаем флаг, что это был последний блок для расжатия
                        //чтобы прекратить while
                        if (BitConverter.ToInt32(lenghtBuffer, 0) < size)
                            notLast = false;

                        data[proc] = new byte[BitConverter.ToInt32(lenghtBuffer, 0)];
                        
                        int[] sender = new []{proc, BitConverter.ToInt32(lenghtBuffer, 0) };
                        //Создаем и запускаем птоки
                        threadMassive[proc] = new Thread(Decompression);
                        threadMassive[proc].Start(sender);
                    }

                    for (int p = 0; p < processorCount && threadMassive[p] != null && !_cancelled;)
                    {
                        //Ждем поток
                        threadMassive[p].Join();
                        if (threadMassive[p].ThreadState == ThreadState.Stopped)
                        {
                            resultSourceStream.Write(data[p], 0, data[p].Length);
                            p++;
                        }
                    }
                }

                resultSourceStream.Close();
                inputSourceStream.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR:" + ex.Message);
                Console.WriteLine(ex.StackTrace);
                //Console.ReadKey(true);
            }
        }
        

        //Жмем
        public static void Compression(object i)
        {
                using (MemoryStream memoryStream = new MemoryStream(data[(int) i].Length))
                {
                    using (GZipStream compressionStream = new GZipStream(memoryStream, CompressionMode.Compress))
                    {
                        compressionStream.Write(data[(int) i], 0, data[(int) i].Length);
                    }
                    compressedData[(int) i] = memoryStream.ToArray();
                    Console.WriteLine("Здесь был: {0}", compressedData[(int) i].Length.ToString());
                }
        }

        
        [Test]
        public void Copy(Stream inputStream, string outputFile)
        {
            using (var outputStream = File.OpenWrite(outputFile))
            {
                const int bufferSize = 4096;
                while (inputStream.Position < inputStream.Length)
                {
                    byte[] data = new byte[bufferSize];
                    int amountRead = inputStream.Read(data, 0, bufferSize);
                    outputStream.Write(data, 0, amountRead);

                }
                outputStream.Flush();
            }
        }



        //Не жмем
        public static void Decompression(object i)
        {
            int count = ((int[])i)[0];
            int leghtBuffer = ((int[]) i)[1];

            using (MemoryStream memoryStream = new MemoryStream(compressedData[count]))
            {
                 //Поток для записи восстановленного файла
                 using (GZipStream decompressionStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                 {
                     try
                     {
                         decompressionStream.Read(data[count], 0, leghtBuffer);//dataArray[count].Length
                     }
                     catch (Exception ex)
                     {
                        
                     }
                 }
                 Console.WriteLine("Здесь был {0} ", data[count].Length.ToString());
            }
        }
    }
}
