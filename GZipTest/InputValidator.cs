﻿using System;
using System.IO;

namespace GZipTest
{
    /// <summary>
    /// Предоставляет инструменты для валидации
    /// </summary>
    class InputValidator
    {
        /// <summary>
        /// Валидация входящих параметров
        /// </summary>
        public static void Validate(string[] args)
        {
            if (args.Length == 0)
                throw new Exception("Вы не ввели параметров!\n" + Constants.Help);

            if (args.Length > 3)
                throw new Exception("Вы ввели более трех параметров!\n" + Constants.Help);

            if (args.Length < 3)
                throw new Exception("Вы ввели менее трех параметров!\n" + Constants.Help);

            if (args[1] == null)
                throw new Exception("Необходимо ввести пусть целевого файла!");

            if (args[0] == Constants.Compress && !File.Exists(args[1]))
                throw new Exception("Файл по указанному целевому пути не существует!");
            FileInfo inputSource = new FileInfo(args[1]);

            if (args[2] == null)
                throw new Exception("Необходимо ввести пусть результирующего файла!");
            FileInfo resultSource = new FileInfo(args[2]);

            if(args[0] != Constants.Compress && args[0] != Constants.Decompress)
                throw new Exception("Первым параметром необходимо указать комманду \""+Constants.Compress+"\" или \""+Constants.Decompress+"\"!");

            if(args[0] == Constants.Compress && inputSource.Extension == Constants.Format)
                throw new Exception("Файл по указанному целевому пути уже компрессирован!");

            if (args[0] == Constants.Compress && resultSource.Extension != Constants.Format)
                throw new Exception("Результирующий файл должен иметь формат "+Constants.Format);

            if (args[0] == Constants.Decompress && inputSource.Extension != Constants.Format)
                throw new Exception("Целевой файл должен иметь формат " + Constants.Format);

            if (File.Exists(args[2]))
                throw new Exception("Результирующий файл с таким именем уже существует. Пожалуйста, ведите другое имя.");

            if (args[0] == Constants.Decompress && !File.Exists(args[1]))
                throw new Exception("Файл по указанному целевому пути не существует!");
        }
    }
}