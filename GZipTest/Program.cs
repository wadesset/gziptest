﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GZipTest
{
    class Program
    {
        private delegate void compress(string inputSource, string resultSource);
        private static DateTime startTime;
        private static DateTime endTime;


        /// <summary>
        /// Точка входа
        /// </summary>
        /// <param name="args">Параметры ввода</param>
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(Constants.Help);
                //Регистрируем событие Ctrl + C
                Console.CancelKeyPress += new ConsoleCancelEventHandler(ExitEvent);
                //Проверяем входящие параметры
                InputValidator.Validate(args);
                Dictionary<string, Delegate> dict = new Dictionary<string, Delegate>();
                //Divider - содержит в себе метод сжатия
                dict.Add("compress", new compress(Selector.Divider));
                //Joiner - содержит в себе метод расжатия
                dict.Add("decompress", new compress(Selector.Joiner));

                foreach (var o in dict.Where(f => f.Key == args[0]))
                {
                    startTime = DateTime.Now;
                    o.Value.DynamicInvoke(args[1], args[2]);
                    
                }
                endTime = DateTime.Now;
                
                Console.WriteLine("\r\nПроцесс сжатия/расжатия прошел успешно!");
                Console.WriteLine("Время выполнения: {0}", endTime - startTime);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey(true);
            }
            catch (Exception e)
            {
                Console.WriteLine("\r\nПроцесс сжатия/расжатия прошел неуспешно!");
                Console.WriteLine("Время выполнения: {0}", endTime - startTime);
                Console.WriteLine(e.Message);
                Console.WriteLine("Нажмите любую клавишу...");
                Console.ReadKey();
            }
        }

        /// <summary>
        ///Ctr + C событие 
        /// </summary>
        private static void ExitEvent(object sender, ConsoleCancelEventArgs args)
        {
            if (args.SpecialKey == ConsoleSpecialKey.ControlC)
            {
                Console.WriteLine("\r\nСовершен выход из программы");
                args.Cancel = true;
                Selector.SetExitState();
            }
        }
    }
}